﻿namespace ExchangeRatesApp
{
    /// <summary>
    /// Класс для представления валютного курса.
    /// </summary>
    public class ExchangeRate
    {
        #region Поля и свойства
        public string Currency { get; }
        public decimal Rate { get; }

        #endregion

        #region Конструкторы
        public ExchangeRate(string currency, decimal rate)
        {
            this.Currency = currency;
            this.Rate = rate;
        }

        #endregion
    }
}