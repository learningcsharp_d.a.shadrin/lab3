﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;

namespace ExchangeRatesApp
{
    /// <summary>
    /// Класс для получения данных о валютных курсах.
    /// </summary>
    public class ExchangeRateScraper
    {
        #region Константы

        private const string StartPageLink = @"https://www.cbr.ru/currency_base/daily/";

        #endregion

        #region Методы
        /// <summary>
        /// Получить список валютных курсов.
        /// </summary>
        /// <returns>Список валютных курсов.</returns>
        public List<ExchangeRate> GetExchangeRates()
        {
            var exchangeRates = new List<ExchangeRate>();

            var htmlDoc = new HtmlWeb().Load(StartPageLink);

            var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='data']//tr");

            if (rows != null)
            {
                foreach (var row in rows)
                {
                    var cells = row.SelectNodes("td");
                    if (cells != null && cells.Count >= 5)
                    {
                        var currency = cells[1].InnerText;
                        var rate = decimal.Parse(cells[4].InnerText);
                        exchangeRates.Add(new ExchangeRate(currency, rate));
                    }
                }
            }

            return exchangeRates;
        }
        #endregion
    }
}

