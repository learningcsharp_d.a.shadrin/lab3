﻿using System;

namespace ExchangeRatesApp
{
    /// <summary>
    /// Класс для получения данных о разнице валютных курсов.
    /// </summary>
    public class ExchangeRateAnalyzer
    {
        
        public delegate void OutputDelegate(string message);

        #region Поля
        /// <summary>
        /// Событие, которое возникает при обнаружении разницы в валютных курсах.
        /// </summary>
        public event EventHandler<string> ExchangeRateDifferenceEvent;

        /// <summary>
        /// Делегат, используемый для вывода сообщений.
        /// </summary>
        private readonly OutputDelegate outputDelegate;

        #endregion

        #region Методы
        /// <summary>
        /// Получить разницу валютных курсов.
        /// </summary>
        /// <returns>Разница валютных курсов в процентах.</returns>
        public void AnalyzeExchangeRate(ExchangeRate exchangeRate, decimal expectedRate, decimal percentageThreshold)
        {
            try
            {
                var difference = Math.Abs(expectedRate - exchangeRate.Rate);
                var differencePercentage = (difference / exchangeRate.Rate) * 100;

                if (differencePercentage >= percentageThreshold)
                {
                    var message = $"Курс отличается на {differencePercentage:F2}% от ожидаемого курса.";
                    ExchangeRateDifferenceEvent?.Invoke(this, message);
                    outputDelegate?.Invoke(message);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = $"Произошла ошибка: {ex.Message}";
                outputDelegate?.Invoke(errorMessage);
            }
        }
        #endregion

        #region Конструкторы

        public ExchangeRateAnalyzer(OutputDelegate outputDelegate)
        {
            this.outputDelegate = outputDelegate;
        }

        #endregion
    }
}
