﻿using System;

public class ConsoleMessageOutput
{
    #region Методы
    
    /// <summary>
    /// Класс для вывода сообщений в консоль.
    /// </summary>
    public void OutputMessage(string message)
    {
        Console.WriteLine(message);
    }

    #endregion
}