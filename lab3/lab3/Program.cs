﻿using ExchangeRatesApp;
using System;

/// <summary>
/// Класс Program содержит функцию Main для ввода и вывода данных.
/// </summary>
class Program
{
    static void Main(string[] args)
    {
        Console.Write("Введите количество валют: ");
        var numCurrencies = int.Parse(Console.ReadLine());

        var exchangeRateScraper = new ExchangeRateScraper();
        var exchangeRates = exchangeRateScraper.GetExchangeRates();

        var consoleMessageOutput = new ConsoleMessageOutput();
        var exchangeRateAnalyzer = new ExchangeRateAnalyzer(consoleMessageOutput.OutputMessage);

        for (var i = 0; i < numCurrencies; i++)
        {
            Console.Write($"Введите {i + 1} валюту: ");
            string currency = Console.ReadLine();

            Console.Write($"Введите ожидаемый курс для валюты {currency}: ");
            decimal expectedRate = decimal.Parse(Console.ReadLine());

            exchangeRateAnalyzer.AnalyzeExchangeRate(exchangeRates.Find(rate => rate.Currency == currency), expectedRate, 15);

            if (i < numCurrencies - 1)
            {
                Console.WriteLine("Для перехода к следующей валюте нажмите любую клавишу . . .");
                Console.ReadKey();
            }
            Console.WriteLine("Для завершения нажмите любую клавишу . . .");
        }

        Console.ReadLine();
    }
}
